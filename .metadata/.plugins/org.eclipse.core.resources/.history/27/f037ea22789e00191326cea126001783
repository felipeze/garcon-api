package com.example.garcon.garconapi.resource;

import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.garcon.garconapi.model.Categoria;
import com.example.garcon.garconapi.repository.CategoriaRepository;

/*Controlador REST*/
/*Retorno convertido para JSON, sem necessidade de anotações extras nos métodos*/
@RestController
@RequestMapping("/categorias") /*Mapeamento da requisição = localhost:8080/categorias*/
public class CategoriaResource {
	
	 
	/*Provê instância da Classe CategoriaRepository para CategoriaResource
	de modo que CategoriaResource fica habilitada a invocar seus métodos
	*/
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@GetMapping /*Método GET*/
	public List<Categoria> listar() {
		return categoriaRepository.findAll();
	}
	
	/*201 CREATED*/
	@PostMapping /*Método POST*/
	/*ResponseEntity<T> retorna conteúdo da URI*/
	public ResponseEntity<Categoria> criarCategoria(@RequestBody Categoria categoria, HttpServletResponse response) {
		Categoria categoriaSalva = categoriaRepository.save(categoria);
		
		/*Classe do Spring - Builder*//*A partir da (URL) requisição atual*/
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}") /*Setando codigo da categoria na URi de retorno*/
				.buildAndExpand(categoriaSalva.getCodigo()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		
		/*Retorno do objeto criado - categoria salva*/
		return ResponseEntity.created(uri).body(categoriaSalva);
		
	}
	
	@GetMapping("/{codigo}")/*Variável da URi*/
	/*Passa o código como parâmetro para retornar a categoria correspondente ao parâmetro indicado*/
	@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Categoria não encontrada")
	public Categoria buscarPeloCodigo(@PathVariable Long codigo) {
		
		if(codigo != null) {
			try {
				return categoriaRepository.findOne(codigo);
			} catch (Exception e) {
				// TODO: handle exception
				
			}
		}
		return null;
		
	}

}
