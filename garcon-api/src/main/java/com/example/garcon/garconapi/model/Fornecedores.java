package com.example.garcon.garconapi.model;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="fornecedores")
public class Fornecedores {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idfornecedor;
	
	@NotNull
	@Size(max=50)
	private String razaosocial;
	
	@Embedded
	@Valid
	private Endereco endereco;
	
	@Size(max=10)
	private String telefonefixo;
	
	@Size(max=11)
	private String telefonecelular;
	
	@Size(max=10)
	private String contato;
	
	@Size(max=10)
	private String funcao;
	
	@NotNull
	@Size(max=18)
	private String cnpj;
	
	@Size(max=20)
	private String inscricaoestadual;
	
	@Size(max=100)
	private String ramoatividade;
	
	@NotNull
	private Boolean status;

	public Long getIdfornecedor() {
		return idfornecedor;
	}

	public void setIdfornecedor(Long idfornecedor) {
		this.idfornecedor = idfornecedor;
	}

	public String getRazaosocial() {
		return razaosocial;
	}

	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getTelefonefixo() {
		return telefonefixo;
	}

	public void setTelefonefixo(String telefonefixo) {
		this.telefonefixo = telefonefixo;
	}

	public String getTelefonecelular() {
		return telefonecelular;
	}

	public void setTelefonecelular(String telefonecelular) {
		this.telefonecelular = telefonecelular;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getFuncao() {
		return funcao;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getInscricaoestadual() {
		return inscricaoestadual;
	}

	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}

	public String getRamoatividade() {
		return ramoatividade;
	}

	public void setRamoatividade(String ramoatividade) {
		this.ramoatividade = ramoatividade;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	
	@JsonIgnore
	@Transient
	public Boolean isInativo() {
		return !this.status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idfornecedor == null) ? 0 : idfornecedor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fornecedores other = (Fornecedores) obj;
		if (idfornecedor == null) {
			if (other.idfornecedor != null)
				return false;
		} else if (!idfornecedor.equals(other.idfornecedor))
			return false;
		return true;
	}
	
	
	
	
}
