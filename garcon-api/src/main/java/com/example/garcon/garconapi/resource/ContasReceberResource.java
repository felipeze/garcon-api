package com.example.garcon.garconapi.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.garcon.garconapi.event.RecursoCriadoEvent;
import com.example.garcon.garconapi.model.ContasReceber;
import com.example.garcon.garconapi.repository.ContasReceberRepository;

@RestController
@RequestMapping("/contasreceber")
public class ContasReceberResource {
	
	@Autowired
	private ContasReceberRepository contasReceberRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher; 
	
	
	@GetMapping
	public List<ContasReceber> listarContasReceber(){
		return contasReceberRepository.findAll();
	}
	
	@GetMapping("/{idContaReceber}")
	public ResponseEntity<ContasReceber> retornaContaReceber(@PathVariable Long idContaReceber){
		ContasReceber retornaConta = contasReceberRepository.getOne(idContaReceber);
		
		if(retornaConta != null) {
			return ResponseEntity.ok().body(retornaConta);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	/*POST*/
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ContasReceber> criarContaReceber(@Valid @RequestBody ContasReceber contaReceber, HttpServletResponse response){
		
		ContasReceber contaCriada = contasReceberRepository.save(contaReceber);
		
		publisher.publishEvent(new RecursoCriadoEvent(this, response, contaCriada.getIdContaReceber()));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(contaCriada);
		
	}
	
	
	/*DELETE*/
	@DeleteMapping("/({idContaReceber})")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void apagarContaReceber(@PathVariable Long idContaReceber) {
		this.contasReceberRepository.delete(idContaReceber);
	}

}
