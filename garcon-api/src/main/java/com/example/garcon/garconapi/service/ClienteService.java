package com.example.garcon.garconapi.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.garcon.garconapi.model.Cliente;
import com.example.garcon.garconapi.repository.ClienteRepository;


@Service/*Serviço do Spring*/
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	public Cliente atualizarCliente(Long idcliente, Cliente cliente) {
		
		Cliente clienteAtualizado = buscaClientePeloCodigo(idcliente);
		
		BeanUtils.copyProperties(cliente, clienteAtualizado, "idcliente");/*Passa os dados do Object cliente para o Object clienteSalvo*/
		return clienteRepository.save(clienteAtualizado);/*Salva as alterações*/
		
	}

	private Cliente buscaClientePeloCodigo(Long idcliente) {
		Cliente clienteAtualizado = this.clienteRepository.findOne(idcliente);/*Retorna o cliente a partir do id*/
		
		if(clienteAtualizado == null) {
			/*Valida se o idcliente passado existe, SE não existir retorna 404 através de exception*/
			throw new EmptyResultDataAccessException(1);
		}
		return clienteAtualizado;
	}

	public void atualizarStatus(Long idcliente, Boolean statuscliente) {
		
		Cliente clienteAtualizado = buscaClientePeloCodigo(idcliente);
		clienteAtualizado.setStatus(statuscliente);
		clienteRepository.save(clienteAtualizado);
		
	}
}
