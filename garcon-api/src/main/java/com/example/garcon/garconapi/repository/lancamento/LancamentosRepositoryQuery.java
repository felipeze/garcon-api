package com.example.garcon.garconapi.repository.lancamento;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.garcon.garconapi.model.Lancamentos;
import com.example.garcon.garconapi.repository.filter.LancamentosFilter;

public interface LancamentosRepositoryQuery {
	
	public Page<Lancamentos> filtrarLancamentos(LancamentosFilter lancamentoFilter, Pageable pageable);

}
