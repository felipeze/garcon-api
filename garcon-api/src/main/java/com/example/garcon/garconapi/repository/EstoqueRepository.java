package com.example.garcon.garconapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.garcon.garconapi.model.Estoque;

public interface EstoqueRepository extends JpaRepository<Estoque, Long>{

}
