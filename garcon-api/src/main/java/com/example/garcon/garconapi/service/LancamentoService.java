package com.example.garcon.garconapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.garcon.garconapi.model.Fornecedores;
import com.example.garcon.garconapi.model.Lancamentos;
import com.example.garcon.garconapi.repository.FornecedoresRepository;
import com.example.garcon.garconapi.repository.LancamentosRepository;
import com.example.garcon.garconapi.service.exception.FornecedorInativoOuInexistenteException;

@Service
public class LancamentoService {
	
	@Autowired
	private FornecedoresRepository fornecedorRepository;
	
	@Autowired
	private LancamentosRepository lancamentoRepository;

	public Lancamentos saveLancamento(Lancamentos lancamento) {
		
		Fornecedores fornecedor = fornecedorRepository.findOne(lancamento.getFornecedor().getIdfornecedor());
		
		if(fornecedor == null || fornecedor.isInativo()) {
			throw new FornecedorInativoOuInexistenteException();
		}else {
			return lancamentoRepository.save(lancamento);
		}
		
	}

}
