package com.example.garcon.garconapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.garcon.garconapi.model.ContasReceber;

public interface ContasReceberRepository extends JpaRepository<ContasReceber, Long>{

}
