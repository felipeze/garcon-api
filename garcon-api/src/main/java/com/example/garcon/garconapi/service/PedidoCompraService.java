package com.example.garcon.garconapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.garcon.garconapi.model.Categoria;
import com.example.garcon.garconapi.model.Fornecedores;
import com.example.garcon.garconapi.model.Lancamentos;
import com.example.garcon.garconapi.model.PedidoCompra;
import com.example.garcon.garconapi.repository.FornecedoresRepository;
import com.example.garcon.garconapi.repository.PedidoCompraRepository;
import com.example.garcon.garconapi.service.exception.FornecedorInativoOuInexistenteException;

@Service
public class PedidoCompraService {

	@Autowired
	private FornecedoresRepository fornecedorRepository;
	
	@Autowired
	private PedidoCompraRepository pedidoCompraRepository;

	
	public PedidoCompra criarPedidoCompra(PedidoCompra pedidoCompra){
		
		Lancamentos novoLancamento = new Lancamentos();
		
		Long codigo = (long) 17;
		
		Fornecedores fornecedor = fornecedorRepository.findOne(pedidoCompra.getFornecedor().getIdfornecedor());
		
		Categoria categoria = new Categoria();
		
		categoria.setCodigo(codigo);
		
		novoLancamento.setData_vencimento(pedidoCompra.getData_entregaprev());
		novoLancamento.setDescricao(pedidoCompra.getCondicao_pagamento()+pedidoCompra.getData_compra());
		novoLancamento.setFornecedor(fornecedorRepository.findOne(pedidoCompra.getFornecedor().getIdfornecedor()));
		novoLancamento.setValor_lancamento(pedidoCompra.getValor_compra());
		novoLancamento.setValor_desconto(pedidoCompra.getValor_desconto());
		novoLancamento.setCategoria(categoria);
		novoLancamento.setNum_pedido_compra("PC");
		
		pedidoCompra.setLancamento(novoLancamento);
		
		if(fornecedor == null || fornecedor.isInativo()) {
			throw new FornecedorInativoOuInexistenteException();
		}else {
			return pedidoCompraRepository.save(pedidoCompra);
		}
	}

}
