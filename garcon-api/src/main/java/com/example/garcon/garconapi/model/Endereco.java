package com.example.garcon.garconapi.model;

import javax.persistence.Embeddable;
import javax.validation.constraints.Size;


@Embeddable
public class Endereco {
	
	@Size(max=40)
	private String rua;
	
	@Size(max=10)
	private String numero;
	
	@Size(max=30)
	private String bairro;
	
	@Size(max=40)
	private String cidade;
	
	@Size(max=25)
	private String estado;
	
	@Size(max=2)
	private String uf;
	
	@Size(max=9)
	private String cep;
	
	@Size(max=20)
	private String complemento;
	
	

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}
	
	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	
	public Integer complementoTamanho() {
		return complemento.length();
	}

	

}
