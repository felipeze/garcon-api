package com.example.garcon.garconapi.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="lancamentos")
public class Lancamentos {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idlancamento;
	
	@NotNull
	@Size(max=50)
	private String descricao;      
	
	@NotNull
	@Column(name="data_vencimento")
	@JsonFormat(pattern =  "dd/MM/yyyy")
	private Date data_vencimento;
	
	@Column(name="data_pagamento")
	@JsonFormat(pattern =  "dd/MM/yyyy")
	private Date data_pagamento;
	
	private BigDecimal valor_lancamento;
	
	private BigDecimal valor_desconto;
	
	@Size(max=100)
	private String observacao;

	@NotNull
	@ManyToOne
	@JoinColumn(name="codigo_categoria")
	private Categoria categoria;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="codigo_fornecedor")
	private Fornecedores fornecedor;
	
	@Size(max=20)
	private String num_pedido_compra;
	

	public Long getIdlancamento() {
		return idlancamento;
	}

	public void setIdlancamento(Long idlancamento) {
		this.idlancamento = idlancamento;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getData_vencimento() {
		return data_vencimento;
	}

	public void setData_vencimento(Date data_vencimento) {
		this.data_vencimento = data_vencimento;
	}

	public Date getData_pagamento() {
		return data_pagamento;
	}

	public void setData_pagamento(Date data_pagamento) {
		this.data_pagamento = data_pagamento;
	}

	public BigDecimal getValor_lancamento() {
		return valor_lancamento;
	}

	public void setValor_lancamento(BigDecimal valor_lancamento) {
		this.valor_lancamento = valor_lancamento;
	}

	public BigDecimal getValor_desconto() {
		return valor_desconto;
	}

	public void setValor_desconto(BigDecimal valor_desconto) {
		this.valor_desconto = valor_desconto;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}


	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Fornecedores getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedores fornecedor) {
		this.fornecedor = fornecedor;
	}
	

	public String getNum_pedido_compra() {
		return num_pedido_compra;
	}

	public void setNum_pedido_compra(String num_pedido_compra) {
		this.num_pedido_compra = num_pedido_compra;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idlancamento == null) ? 0 : idlancamento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lancamentos other = (Lancamentos) obj;
		if (idlancamento == null) {
			if (other.idlancamento != null)
				return false;
		} else if (!idlancamento.equals(other.idlancamento))
			return false;
		return true;
	}
	
	

}
