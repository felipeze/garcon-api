package com.example.garcon.garconapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.garcon.garconapi.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long>{
	
	
}
