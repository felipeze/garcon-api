package com.example.garcon.garconapi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;



@Entity
@Table(name="clientes")
public class Cliente {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idcliente;
	
	@NotNull
	@Size(max=100)
	private String nomecliente;
	
	@Column(name="dtnascimento")
	@JsonFormat(pattern =  "dd/MM/yyyy")
	private Date dtnascimento;
		
	@Embedded
	@Valid
	private Endereco endereco;
	
	@NotNull
	@Size(max=14)
	private String cpf;
	
	@NotNull
	private Boolean status;

	public Long getIdCliente() {
		return idcliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idcliente = idCliente;
	}

	public String getNomeCliente() {
		return nomecliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomecliente = nomeCliente;
	}

	public Date getDtNascimento() {
		return dtnascimento;
	}

	public void setDtNascimento(Date dtNascimento) {
		this.dtnascimento = dtNascimento;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idcliente == null) ? 0 : idcliente.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (idcliente == null) {
			if (other.idcliente != null)
				return false;
		} else if (!idcliente.equals(other.idcliente))
			return false;
		return true;
	}	

}
