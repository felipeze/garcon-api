package com.example.garcon.garconapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.garcon.garconapi.model.Fornecedores;


public interface FornecedoresRepository extends JpaRepository<Fornecedores, Long>{

}
