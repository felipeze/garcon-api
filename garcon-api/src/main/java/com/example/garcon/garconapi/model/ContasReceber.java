package com.example.garcon.garconapi.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="contas_receber")
public class ContasReceber {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idContaReceber;
	
	@NotNull
	@Size(max=50)
	private String descricao;
	
	@NotNull
	@Column(name="data_vencimento")
	@JsonFormat(pattern =  "dd/MM/yyyy")
	private Date dataVencimento;
	
	@Column(name="data_pagamento")
	@JsonFormat(pattern =  "dd/MM/yyyy")
	private Date dataPagamento;
	
	private BigDecimal valorLancamento;
	
	private BigDecimal valorDesconto;
	
	@Size(max=100)
	private String observacao;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="codigo_categoria")
	private Categoria codigoCategoria;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="codigo_cliente")
	private Cliente codigoCliente;

	public Long getIdContaReceber() {
		return idContaReceber;
	}

	public void setIdContaReceber(Long idContaReceber) {
		this.idContaReceber = idContaReceber;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public BigDecimal getValorLancamento() {
		return valorLancamento;
	}

	public void setValorLancamento(BigDecimal valorLancamento) {
		this.valorLancamento = valorLancamento;
	}

	public BigDecimal getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(BigDecimal valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Categoria getCodigoCategoria() {
		return codigoCategoria;
	}

	public void setCodigoCategoria(Categoria codigoCategoria) {
		this.codigoCategoria = codigoCategoria;
	}

	public Cliente getCodigoCliente() {
		return codigoCliente;
	}

	public void setCodigoCliente(Cliente codigoCliente) {
		this.codigoCliente = codigoCliente;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idContaReceber == null) ? 0 : idContaReceber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContasReceber other = (ContasReceber) obj;
		if (idContaReceber == null) {
			if (other.idContaReceber != null)
				return false;
		} else if (!idContaReceber.equals(other.idContaReceber))
			return false;
		return true;
	}
	
	
}
