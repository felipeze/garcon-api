package com.example.garcon.garconapi.resource;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.garcon.garconapi.event.RecursoCriadoEvent;
import com.example.garcon.garconapi.exceptionhandler.GarconApiExceptionHandler.Erro;
import com.example.garcon.garconapi.model.PedidoCompra;
import com.example.garcon.garconapi.repository.PedidoCompraRepository;
import com.example.garcon.garconapi.service.PedidoCompraService;
import com.example.garcon.garconapi.service.exception.FornecedorInativoOuInexistenteException;

@RestController
@RequestMapping("/pedidocompra")
public class PedidoCompraResource {
	
	@Autowired
	private PedidoCompraRepository pedidoCompraRepository;
	
	@Autowired
	private PedidoCompraService pedidoCompraService;
	
	@Autowired
	private ApplicationEventPublisher publisher;

	@Autowired
	private MessageSource messageSource;
	
	
	@GetMapping
	public List<PedidoCompra> listarPedidosCompras(){
		return pedidoCompraRepository.findAll();
	} 
	
	
	@GetMapping("/{idPedidoCompra}")
	public ResponseEntity<PedidoCompra> retornaPedidoCompra(@PathVariable Long idPedidoCompra){
		
		PedidoCompra retornaPedidoCompra = pedidoCompraRepository.findOne(idPedidoCompra);
		
		if(retornaPedidoCompra != null) {
			return ResponseEntity.ok().body(retornaPedidoCompra);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	private ResponseEntity<PedidoCompra> criarPedidoCompra(@Valid @RequestBody PedidoCompra pedidoCompra, HttpServletResponse response) {
		
		PedidoCompra pedidoCompraCriado = pedidoCompraService.criarPedidoCompra(pedidoCompra);
		
		publisher.publishEvent(new RecursoCriadoEvent(this, response, pedidoCompraCriado.getCod_compra()));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(pedidoCompraCriado);
		
	}

	/*Capturando Exceção SE o fornecedor for NULL ou Inativo*/
	@ExceptionHandler({ FornecedorInativoOuInexistenteException.class })
	public ResponseEntity<Object> handleFornecedorInativoOuInexistenteException(FornecedorInativoOuInexistenteException ex){
		String mensagemUsuario = messageSource.getMessage("fornecedor-inexistente-ou-inativo", null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		
		return ResponseEntity.badRequest().body(erros);
	}
	
	/*DELETE*/
	@DeleteMapping("/{cod_compra}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void apagarPedidoCompra(@PathVariable Long cod_compra){
		this.pedidoCompraRepository.delete(cod_compra);
	}
	


}
