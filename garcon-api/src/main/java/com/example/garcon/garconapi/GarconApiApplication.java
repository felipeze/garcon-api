package com.example.garcon.garconapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GarconApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GarconApiApplication.class, args);
	}

}
