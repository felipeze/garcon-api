package com.example.garcon.garconapi.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.garcon.garconapi.event.RecursoCriadoEvent;
import com.example.garcon.garconapi.model.Fornecedores;
import com.example.garcon.garconapi.repository.FornecedoresRepository;
import com.example.garcon.garconapi.service.FornecedorService;

@RestController
@RequestMapping("/fornecedores")
public class FornecedoresResource {
	
	@Autowired
	private FornecedoresRepository fornecedorRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private FornecedorService fornecedorService;
	
	/*GET - 200 OK*/
	@GetMapping
	 public List<Fornecedores> listarFornecedores(){
		 return fornecedorRepository.findAll();
	 }
	
	/*GET - 200 OK*/
	@GetMapping("/{idfornecedor}")
	public ResponseEntity<Fornecedores> localizaFornecedor(@PathVariable Long idfornecedor) {
		Fornecedores retornaFornecedor =  fornecedorRepository.findOne(idfornecedor);
		
		if(retornaFornecedor != null) {
			return ResponseEntity.ok().body(retornaFornecedor);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	/*POST - 201*/
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Fornecedores> criarFornecedor(@Valid @RequestBody Fornecedores fornecedor, HttpServletResponse response){
		
		Fornecedores fornecedorSalvo = fornecedorRepository.save(fornecedor);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, fornecedorSalvo.getIdfornecedor()));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(fornecedorSalvo);
	}
	
	
	/*DELETE - 204*/	
	@DeleteMapping("/{idfornecedor}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void apagarFornecedor(@PathVariable Long idfornecedor) {
		this.fornecedorRepository.delete(idfornecedor);
	}
	
	
	/*Atualiza dados do fornecedor*/
	@PutMapping("/{idfornecedor}")
	public ResponseEntity<Fornecedores> atualizaFornecedor(@PathVariable Long idfornecedor, @Valid @RequestBody Fornecedores fornecedor){
		
		Fornecedores fornecedorSalvo = this.fornecedorService.atualizarFornecedores(idfornecedor, fornecedor);
		return ResponseEntity.ok(fornecedorSalvo); /*Retorna 200 OK*/
	}
	
	/*Atualização parcial - Apenas STATUS*/
	@PutMapping("{idfornecedor}/status")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void atualizarStatusFornecedor(@PathVariable Long idfornecedor, @RequestBody Boolean statusfornecedor) {
		fornecedorService.atualizarStatusFornecedor(idfornecedor, statusfornecedor);
	}
	
}
