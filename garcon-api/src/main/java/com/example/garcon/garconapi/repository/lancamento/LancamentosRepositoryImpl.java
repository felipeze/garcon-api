package com.example.garcon.garconapi.repository.lancamento;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.example.garcon.garconapi.model.Lancamentos;
import com.example.garcon.garconapi.model.Lancamentos_;
import com.example.garcon.garconapi.repository.filter.LancamentosFilter;

public class LancamentosRepositoryImpl implements LancamentosRepositoryQuery{
	
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Lancamentos> filtrarLancamentos(LancamentosFilter lancamentoFilter, Pageable pageable){
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Lancamentos> criteria = builder.createQuery(Lancamentos.class);
		
		//Pega os atributos que serão atribuidos ao filtro
		Root<Lancamentos> root = criteria.from(Lancamentos.class);
		
		//Criar as Restrições
		Predicate[] predicates = criarRestricoes(lancamentoFilter, builder, root);  
		criteria.where(predicates);
		
		
		TypedQuery<Lancamentos> query = manager.createQuery(criteria);
		
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<Lancamentos>(query.getResultList(), pageable, total(lancamentoFilter));
	}



	private Predicate[] criarRestricoes(LancamentosFilter lancamentoFilter, CriteriaBuilder builder,
			Root<Lancamentos> root) {
		
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if(!StringUtils.isEmpty(lancamentoFilter.getDescricao())) {
			predicates.add(
					builder.like(
					builder.lower(root.get(Lancamentos_.descricao)),
					"%" + lancamentoFilter.getDescricao().toLowerCase() + "%"
					));
		}
		
		if(lancamentoFilter.getData_vencimentoDe() != null) {
			predicates.add(
					builder.greaterThanOrEqualTo(root.get(Lancamentos_.data_vencimento),
					lancamentoFilter.getData_vencimentoDe()));
		}
		
		if(lancamentoFilter.getData_vencimentoAte() != null) {
			predicates.add(
					builder.lessThanOrEqualTo(root.get(Lancamentos_.data_vencimento),
					lancamentoFilter.getData_vencimentoAte()));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	

	private void adicionarRestricoesDePaginacao(TypedQuery<Lancamentos> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
		
	}
	
	private long total(LancamentosFilter lancamentoFilter) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		
		Root<Lancamentos> root = criteria.from(Lancamentos.class);
		
		Predicate[] predicates = criarRestricoes(lancamentoFilter, builder, root);
		
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		
		return manager.createQuery(criteria).getSingleResult();
	}
	
	


}














