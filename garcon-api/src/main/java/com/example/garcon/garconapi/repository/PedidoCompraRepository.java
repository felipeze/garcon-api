package com.example.garcon.garconapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.garcon.garconapi.model.PedidoCompra;

public interface PedidoCompraRepository extends JpaRepository<PedidoCompra, Long>{

}
