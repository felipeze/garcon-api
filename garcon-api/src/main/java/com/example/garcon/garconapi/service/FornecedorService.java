package com.example.garcon.garconapi.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.garcon.garconapi.model.Fornecedores;
import com.example.garcon.garconapi.repository.FornecedoresRepository;

@Service/*Serviço do Spring*/
public class FornecedorService {
	
	@Autowired
	private FornecedoresRepository fornecedorRepository;
	
	
	public Fornecedores atualizarFornecedores(Long idfornecedor, Fornecedores fornecedor) {
		
		Fornecedores fornecedorAtualizado = buscaFornecedorPeloCodigo(idfornecedor);
		
		BeanUtils.copyProperties(fornecedor, fornecedorAtualizado, "idfornecedor");
		return fornecedorRepository.save(fornecedorAtualizado);/*Salva as alterações*/
		
	}
	
	
	public Fornecedores buscaFornecedorPeloCodigo(Long idfornecedor) {
		Fornecedores fornecedorAtualizado = this.fornecedorRepository.findOne(idfornecedor);/*Retorna o fornecedor a partir do id*/
		
		if(fornecedorAtualizado == null) {
			/*Valida se o idfornecedor passado existe, SE não existir retorna 404 através de exception*/
			throw new EmptyResultDataAccessException(1);
		}
		return fornecedorAtualizado;
	}
	
	
	public void atualizarStatusFornecedor(Long idfornecedor, Boolean statusfornecedor) {
		
		Fornecedores fornecedorAtualizado = buscaFornecedorPeloCodigo(idfornecedor);
		fornecedorAtualizado.setStatus(statusfornecedor);
		fornecedorRepository.save(fornecedorAtualizado);
		
	}

}
