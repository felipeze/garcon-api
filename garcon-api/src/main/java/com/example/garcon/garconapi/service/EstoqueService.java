package com.example.garcon.garconapi.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.garcon.garconapi.model.Estoque;
import com.example.garcon.garconapi.repository.EstoqueRepository;

@Service
public class EstoqueService {
	
	@Autowired
	private EstoqueRepository estoqueRepository;
	
	
	public Estoque atualizarEstoque(Long cod_estoque, Estoque estoque) {
		
		Estoque estoqueAtualizado = buscaEstoquePeloCodigo(cod_estoque);
		
		BeanUtils.copyProperties(estoque, estoqueAtualizado, "cod_estoque");
		return estoqueRepository.save(estoqueAtualizado);/*Salva as alterações*/
		
	}


	private Estoque buscaEstoquePeloCodigo(Long cod_estoque) {
		Estoque estoqueAtualizado = this.estoqueRepository.findOne(cod_estoque);/*Retorna o fornecedor a partir do id*/
		
		if(estoqueAtualizado == null) {
			/*Valida se o idfornecedor passado existe, SE não existir retorna 404 através de exception*/
			throw new EmptyResultDataAccessException(1);
		}
		return estoqueAtualizado;
	}

}
