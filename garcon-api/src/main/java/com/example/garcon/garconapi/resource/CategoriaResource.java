package com.example.garcon.garconapi.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.garcon.garconapi.event.RecursoCriadoEvent;
import com.example.garcon.garconapi.model.Categoria;
import com.example.garcon.garconapi.repository.CategoriaRepository;

/*Controlador REST*/
/*Retorno convertido para JSON, sem necessidade de anotações extras nos métodos*/

@RestController
@RequestMapping("/categorias") /*Mapeamento da requisição = localhost:8080/categorias*/
public class CategoriaResource {
	
	 
	/*Provê instância da Classe CategoriaRepository para CategoriaResource
	de modo que CategoriaResource fica habilitada a invocar seus métodos*/
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher; /*Pubicador de evetos de aplicação*/
	
	/*Método GET*/
	@GetMapping
	public List<Categoria> listar() {
		return categoriaRepository.findAll();
	}
	
	/*201 CREATED*/
	 /*Método POST*/
	/*ResponseEntity<T> retorna conteúdo da URI*/
	@PostMapping
	public ResponseEntity<Categoria> criarCategoria(@Valid @RequestBody Categoria categoria, HttpServletResponse response) {
		
		Categoria categoriaSalva = categoriaRepository.save(categoria); /*Retorna o código*/
		
		/*this = Objeto que gerou o Event (RecursoCriadoEvent)*/		
		publisher.publishEvent(new RecursoCriadoEvent(this, response, categoriaSalva.getCodigo()));
		return ResponseEntity.status(HttpStatus.CREATED).body(categoriaSalva);
		/*Retorno do objeto criado - categoria salva*/
		
	}
	
	/*Variável da URi*/
	/*Passa o código como parâmetro para retornar a categoria correspondente ao parâmetro indicado*/
	
	@GetMapping("/{codigo}")
	public ResponseEntity<Categoria> buscarPeloCodigo(@PathVariable Long codigo) {
	  Categoria categoria = categoriaRepository.findOne(codigo);
	  
	  if(categoria != null) {
		  return ResponseEntity.ok().body(categoria);
	  }else {
		  return ResponseEntity.notFound().build();
	  }
	  
	}		
}
