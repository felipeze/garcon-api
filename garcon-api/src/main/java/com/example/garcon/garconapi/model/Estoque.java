package com.example.garcon.garconapi.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="estoque")
public class Estoque {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long cod_estoque;
	
	@NotNull
	@OneToOne (cascade = CascadeType.PERSIST )
	@JoinColumn(name = "cod_produto")
	private Produtos produto;
	
	@Column(name="data_entrada")
	@JsonFormat(pattern =  "dd/MM/yyyy")
	private Date data_entrada;
	
	@Column(name="data_inventario")
	@JsonFormat(pattern =  "dd/MM/yyyy")
	private Date data_inventario;
	
	@Column(name="data_validade")
	@JsonFormat(pattern =  "dd/MM/yyyy")
	private Date data_validade;
	
	private BigDecimal quant_entrada;
	
	private BigDecimal quant_saida;
	
	

	public Long getCod_estoque() {
		return cod_estoque;
	}

	public void setCod_estoque(Long cod_estoque) {
		this.cod_estoque = cod_estoque;
	}

	public Produtos getProduto() {
		return produto;
	}

	public void setProduto(Produtos produto) {
		this.produto = produto;
	}

	public Date getData_entrada() {
		return data_entrada;
	}

	public void setData_entrada(Date data_entrada) {
		this.data_entrada = data_entrada;
	}

	public Date getData_inventario() {
		return data_inventario;
	}

	public void setData_inventario(Date data_inventario) {
		this.data_inventario = data_inventario;
	}

	public Date getData_validade() {
		return data_validade;
	}

	public void setData_validade(Date data_validade) {
		this.data_validade = data_validade;
	}

	public BigDecimal getQuant_entrada() {
		return quant_entrada;
	}

	public void setQuant_entrada(BigDecimal quant_entrada) {
		this.quant_entrada = quant_entrada;
	}

	public BigDecimal getQuant_saida() {
		return quant_saida;
	}

	public void setQuant_saida(BigDecimal quant_saida) {
		this.quant_saida = quant_saida;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cod_estoque == null) ? 0 : cod_estoque.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estoque other = (Estoque) obj;
		if (cod_estoque == null) {
			if (other.cod_estoque != null)
				return false;
		} else if (!cod_estoque.equals(other.cod_estoque))
			return false;
		return true;
	}
	
}
