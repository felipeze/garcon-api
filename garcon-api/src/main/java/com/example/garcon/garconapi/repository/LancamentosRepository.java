package com.example.garcon.garconapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.garcon.garconapi.model.Lancamentos;
import com.example.garcon.garconapi.repository.lancamento.LancamentosRepositoryQuery;

public interface LancamentosRepository extends JpaRepository<Lancamentos, Long>, LancamentosRepositoryQuery{

}
