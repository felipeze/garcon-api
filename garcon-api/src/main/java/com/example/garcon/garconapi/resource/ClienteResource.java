package com.example.garcon.garconapi.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.garcon.garconapi.event.RecursoCriadoEvent;
import com.example.garcon.garconapi.model.Cliente;
import com.example.garcon.garconapi.repository.ClienteRepository;
import com.example.garcon.garconapi.service.ClienteService;

@RestController
@RequestMapping("/clientes")
public class ClienteResource {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private ApplicationEventPublisher publisher; /*Pubicador de evetos de aplicação*/
	
	
	/*200 OK*/
	@GetMapping
	public List<Cliente> listarClientes() {
		return clienteRepository.findAll();
	}
	
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Cliente> criarCliente(@Valid @RequestBody Cliente cliente, HttpServletResponse response) {
		
		Cliente clienteSalvo = clienteRepository.save(cliente); /*Retorna o código*/
		publisher.publishEvent(new RecursoCriadoEvent(this, response, clienteSalvo.getIdCliente()));
	
		/*Retorna o novo cliente criado*/
		return ResponseEntity.status(HttpStatus.CREATED).body(clienteSalvo);
		
		
	}
	
	
	@GetMapping("/{idcliente}")
	public ResponseEntity<Cliente> buscarCliente(@PathVariable Long idcliente) {
		
		Cliente retornaCliente = clienteRepository.findOne(idcliente);
		
		if(retornaCliente != null) {
			return ResponseEntity.ok().body(retornaCliente);
		}else {
			return ResponseEntity.notFound().build();
		}
		
	}
	
	
	@DeleteMapping("/{idcliente}")
	@ResponseStatus(HttpStatus.NO_CONTENT) /*Retorna 404 e suprime 500*/
	public void apagarCliente(@PathVariable Long idcliente) {
		this.clienteRepository.delete(idcliente);
	}
	
	
	@PutMapping("/{idcliente}")
	public ResponseEntity<Cliente> atualizarCliente(@PathVariable Long idcliente, @Valid @RequestBody Cliente cliente){
		
		Cliente clienteSalvo = this.clienteService.atualizarCliente(idcliente, cliente);
		return ResponseEntity.ok(clienteSalvo);/*Retorna OK*/
	}
	
	
	@PutMapping("/{idcliente}/status")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void atualizaStatusCliente(@PathVariable Long idcliente, @RequestBody Boolean statuscliente) {
		clienteService.atualizarStatus(idcliente, statuscliente);
	}

}
