package com.example.garcon.garconapi.event.listener;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.garcon.garconapi.event.RecursoCriadoEvent;

@Component
public class RecursoCriadoListener implements ApplicationListener<RecursoCriadoEvent>{

	@Override
	public void onApplicationEvent(RecursoCriadoEvent recursoCriadoEvent) {
		
		HttpServletResponse response = recursoCriadoEvent.getResponse();
		Long codigo = recursoCriadoEvent.getCodigo();
		
		adicionarHeaderLocation(response, codigo);
		
	}

	private void adicionarHeaderLocation(HttpServletResponse response, Long codigo) {
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}") 
				.buildAndExpand(codigo).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		/*ServletUriComponentsBuilder Classe do Spring - Builder*//* fromCurrentRequestUri = A partir da (URL) requisição atual*/
		/*buildAndExpand(categoriaSalva.getCodigo()).toUri() = Setando codigo da categoria na URi de retorno*/
		
		/*Setando o IDCLIENTE na URi*/
	}
	

}
