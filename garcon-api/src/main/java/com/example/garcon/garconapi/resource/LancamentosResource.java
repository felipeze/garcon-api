package com.example.garcon.garconapi.resource;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.garcon.garconapi.event.RecursoCriadoEvent;
import com.example.garcon.garconapi.exceptionhandler.GarconApiExceptionHandler.Erro;
import com.example.garcon.garconapi.model.Lancamentos;
import com.example.garcon.garconapi.repository.LancamentosRepository;
import com.example.garcon.garconapi.repository.filter.LancamentosFilter;
import com.example.garcon.garconapi.service.LancamentoService;
import com.example.garcon.garconapi.service.exception.FornecedorInativoOuInexistenteException;

@RestController
@RequestMapping("/lancamentos")
public class LancamentosResource {
	
	@Autowired
	private LancamentosRepository lancamentoRepository;
	
	@Autowired
	private LancamentoService lancamentoService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private MessageSource messageSource;
	
	
	/*GET - 200 OK retorna uma listagem de lancamentos*/
	@GetMapping
	public Page<Lancamentos> pesquisarLancamentos(LancamentosFilter lancamentoFilter, Pageable pageable){
		return lancamentoRepository.filtrarLancamentos(lancamentoFilter, pageable);
	}
	
	
	/*GET - 200 OK retorna um único registro de lançamento*/
	@GetMapping("/{idlancamento}")
	public ResponseEntity<Lancamentos> retornaLancamento(@PathVariable Long idlancamento){
		
		Lancamentos retornaLancamento = lancamentoRepository.findOne(idlancamento);
		
		if(retornaLancamento != null) {
			return ResponseEntity.ok().body(retornaLancamento);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	private ResponseEntity<Lancamentos> cadastraLancamentos(@Valid @RequestBody Lancamentos lancamento, HttpServletResponse response){
		
		Lancamentos lancamentoCriado = lancamentoService.saveLancamento(lancamento);
		
		publisher.publishEvent(new RecursoCriadoEvent(this, response, lancamentoCriado.getIdlancamento()));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(lancamentoCriado);
	}
	
	/*Capturando Exceção SE o fornecedor for NULL ou Inativo*/
	@ExceptionHandler({ FornecedorInativoOuInexistenteException.class })
	public ResponseEntity<Object> handleFornecedorInativoOuInexistenteException(FornecedorInativoOuInexistenteException ex){
		String mensagemUsuario = messageSource.getMessage("fornecedor-inexistente-ou-inativo", null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		
		return ResponseEntity.badRequest().body(erros);
	}
	

	/*DELETE - 204*/	 
	@DeleteMapping("/{idlancamento}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void apagarLancamento(@PathVariable Long idlancamento) {
		this.lancamentoRepository.delete(idlancamento);
	}

}
