package com.example.garcon.garconapi.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="produtos")
public class GrupoProduto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cod_grupo;
	
	private String descricao;
	
	private String categoria;

	public Long getCod_grupo() {
		return cod_grupo;
	}

	public void setCod_grupo(Long cod_grupo) {
		this.cod_grupo = cod_grupo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cod_grupo == null) ? 0 : cod_grupo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoProduto other = (GrupoProduto) obj;
		if (cod_grupo == null) {
			if (other.cod_grupo != null)
				return false;
		} else if (!cod_grupo.equals(other.cod_grupo))
			return false;
		return true;
	}
	

}
