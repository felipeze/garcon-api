package com.example.garcon.garconapi.repository.filter;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class LancamentosFilter {
	
	private String descricao;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date data_vencimentoDe;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date data_vencimentoAte;
	
	
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Date getData_vencimentoDe() {
		return data_vencimentoDe;
	}
	public void setData_vencimentoDe(Date data_vencimentoDe) {
		this.data_vencimentoDe = data_vencimentoDe;
	}
	public Date getData_vencimentoAte() {
		return data_vencimentoAte;
	}
	public void setData_vencimentoAte(Date data_vencimentoAte) {
		this.data_vencimentoAte = data_vencimentoAte;
	}
	
	
	
}
