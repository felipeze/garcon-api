package com.example.garcon.garconapi.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="pedidocompra")
public class PedidoCompra {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long cod_compra;
	
	@Size(max=20)
	private String num_pedido;
	
	@NotNull
	@Column(name="data_compra")
	@JsonFormat(pattern =  "dd/MM/yyyy")
	private Date data_compra;
	
	@Column(name="data_entrega")
	@JsonFormat(pattern =  "dd/MM/yyyy")
	private Date data_entrega;
	
	@NotNull
	@Column(name="data_entregaprev")
	@JsonFormat(pattern =  "dd/MM/yyyy")
	private Date data_entregaprev;
	
	private String condicao_pagamento;
	
	private BigDecimal valor_compra;
	
	private BigDecimal valor_desconto;

	@NotNull
	@ManyToOne
	@JoinColumn(name="codigo_fornecedor")
	private Fornecedores fornecedor;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="cod_produto")
	private Produtos produto;
	
	@OneToOne (cascade = CascadeType.PERSIST )
	@JoinColumn(name = "id_lancamento")
	private Lancamentos lancamento;
	

	public Long getCod_compra() {
		return cod_compra;
	}

	public void setCod_compra(Long cod_compra) {
		this.cod_compra = cod_compra;
	}

	public String getNum_pedido() {
		return num_pedido;
	}

	public void setNum_pedido(String num_pedido) {
		this.num_pedido = num_pedido;		
	}

	public Date getData_compra() {
		return data_compra;
	}

	public void setData_compra(Date data_compra) {
		this.data_compra = data_compra;
	}

	public Date getData_entrega() {
		return data_entrega;
	}

	public void setData_entrega(Date data_entrega) {
		this.data_entrega = data_entrega;
	}
	

	public Date getData_entregaprev() {
		return data_entregaprev;
	}

	public void setData_entregaprev(Date data_entregaprev) {
		this.data_entregaprev = data_entregaprev;
	}

	public String getCondicao_pagamento() {
		return condicao_pagamento;
	}

	public void setCondicao_pagamento(String condicao_pagamento) {
		this.condicao_pagamento = condicao_pagamento;
	}

	public BigDecimal getValor_compra() {
		return valor_compra;
	}

	public void setValor_compra(BigDecimal valor_compra) {
		this.valor_compra = valor_compra;
	}

	public BigDecimal getValor_desconto() {
		return valor_desconto;
	}

	public void setValor_desconto(BigDecimal valor_desconto) {
		this.valor_desconto = valor_desconto;
	}


	public Fornecedores getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedores fornecedor) {
		this.fornecedor = fornecedor;
	}

	public Produtos getProduto() {
		return produto;
	}

	public void setProduto(Produtos produto) {
		this.produto = produto;
	}
	

	public Lancamentos getLancamento() {
		return lancamento;
	}

	public void setLancamento(Lancamentos lancamento) {
		this.lancamento = lancamento;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cod_compra == null) ? 0 : cod_compra.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PedidoCompra other = (PedidoCompra) obj;
		if (cod_compra == null) {
			if (other.cod_compra != null)
				return false;
		} else if (!cod_compra.equals(other.cod_compra))
			return false;
		return true;
	}

}
