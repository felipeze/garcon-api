package com.example.garcon.garconapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.garcon.garconapi.model.Produtos;

public interface ProdutosRepository extends JpaRepository<Produtos, Long>{

}
