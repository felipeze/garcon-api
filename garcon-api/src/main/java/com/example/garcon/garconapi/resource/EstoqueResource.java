package com.example.garcon.garconapi.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.garcon.garconapi.event.RecursoCriadoEvent;
import com.example.garcon.garconapi.model.Estoque;
import com.example.garcon.garconapi.repository.EstoqueRepository;

@RestController
@RequestMapping("/estoque")
public class EstoqueResource {
	
	@Autowired
	private EstoqueRepository estoqueRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher; /*Pubicador de evetos de aplicação*/
	
	/*GET - 200 OK*/
	public List<Estoque> listarEstoque(){
		return estoqueRepository.findAll();
	}
	
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Estoque> criarEstoque(@Valid @RequestBody Estoque estoque, HttpServletResponse response){
		
		Estoque novoEstoque = estoqueRepository.save(estoque);
		
		publisher.publishEvent(new RecursoCriadoEvent(this, response, novoEstoque.getCod_estoque()));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(novoEstoque);
		
	}
	
	
	
	
	
}
