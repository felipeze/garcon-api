package com.example.garcon.garconapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.garcon.garconapi.model.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

}