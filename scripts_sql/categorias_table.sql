CREATE TABLE `categorias` (
	`codigo` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`nome` VARCHAR(60) NOT NULL,
	PRIMARY KEY (`codigo`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=20
;