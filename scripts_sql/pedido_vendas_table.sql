CREATE TABLE `pedidovenda`(
	`cod_venda` INT(11) NOT NULL AUTO_INCREMENT,
	`num_pedido` INT(11) NOT NULL,
	`data_emissao` DATE NULL DEFAULT NULL,
	`data_venda` DATE NOT NULL,
	`data_entrega` DATE NULL DEFAULT NULL,
	`forma_pagamento` VARCHAR(30) NULL DEFAULT NULL,
	`valor_total` DECIMAL(10,2) NULL DEFAULT NULL,
	`valor_desconto` DECIMAL(10,2) NULL DEFAULT NULL,
	`STATUS` INT(1) NOT NULL,
	`codigo_cliente` BIGINT(20) NOT NULL,
	`codigo_produto` BIGINT(20),
	PRIMARY KEY(`cod_venda`),
	INDEX(`codigo_cliente`),
	FOREIGN KEY(`codigo_cliente`)	REFERENCES  clientes(idcliente),
	INDEX(`codigo_produto`),
	FOREIGN KEY(`codigo_produto`)	REFERENCES  produtos(cod_produto)
)
COLLATE='utf8_general_ci'
ENGINE=INNODB DEFAULT CHARSET=utf8