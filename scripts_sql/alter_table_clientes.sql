ALTER TABLE clientes ADD numero INT(7);
ALTER TABLE clientes ADD bairro VARCHAR(30);
ALTER TABLE clientes ADD cidade VARCHAR(40);
ALTER TABLE clientes ADD estado VARCHAR(25);
ALTER TABLE clientes ADD uf CHAR(2);
ALTER TABLE clientes ADD cep VARCHAR(9);
ALTER TABLE clientes ADD complemento VARCHAR(10);
ALTER TABLE clientes DROP cpf;
ALTER TABLE clientes ADD cpf VARCHAR(14);


ALTER TABLE clientes MODIFY complemento VARCHAR(20) AFTER cep;