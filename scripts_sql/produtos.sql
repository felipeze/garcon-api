CREATE TABLE produtos(
	cod_produto BIGINT(20) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	cod_fornecedor BIGINT(20) NOT NULL,
	nome_produto VARCHAR(60),
	descricao VARCHAR(100),
	preco_venda DECIMAL(10,2),
	preco_unitario DECIMAL(10,2),
	validade DATE NULL DEFAULT NULL
)COLLATE='utf8_general_ci'
ENGINE=INNODB DEFAULT CHARSET=UTF8