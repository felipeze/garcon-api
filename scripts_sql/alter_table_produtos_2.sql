ALTER TABLE produtos ADD cod_grupo BIGINT(20);
ALTER TABLE produtos ADD INDEX(cod_grupo);
ALTER TABLE produtos ADD FOREIGN KEY(cod_grupo) REFERENCES grupo_produto(cod_grupo);