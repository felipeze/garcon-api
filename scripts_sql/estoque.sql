CREATE TABLE estoque(
	cod_estoque BIGINT(20) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	cod_produto BIGINT(20) NOT NULL,
	data_entrada DATE NOT NULL,
	data_saida DATE NULL DEFAULT NULL,
	quant_entrada BIGINT(20) NOT NULL,
	quant_saida BIGINT(20) NULL DEFAULT NULL,
	INDEX(cod_produto),
	FOREIGN KEY(cod_produto) REFERENCES produtos(cod_produto)
)COLLATE='utf8_general_ci'
ENGINE=INNODB DEFAULT CHARSET=UTF8