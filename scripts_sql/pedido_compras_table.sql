CREATE TABLE `pedidocompra`(
	`cod_compra` INT(11) NOT NULL AUTO_INCREMENT,
	`numped_cp` INT(11) NOT NULL,
	`data_compra` DATE NULL DEFAULT NULL,
	`data_entrega` DATE NULL DEFAULT NULL,
	`entrega_prevista` DATE NULL DEFAULT NULL,
	`condicao_pagamento` VARCHAR(30) NULL DEFAULT NULL,
	`valor_compra` DECIMAL(10,2) NULL DEFAULT NULL,
	`valor_desconto` DECIMAL(10,2) NULL DEFAULT NULL,
	`STATUS` INT(1) NOT NULL,
	`codigo_fornecedor` BIGINT(20) NOT NULL,
	`cod_produto` BIGINT(20) NOT NULL,
	PRIMARY KEY(`cod_compra`),
	INDEX(`codigo_fornecedor`),
	FOREIGN KEY(`codigo_fornecedor`)	REFERENCES  fornecedores(idfornecedor),
	INDEX(`cod_produto`),
	FOREIGN KEY(`cod_produto`)	REFERENCES  produtos(cod_produto)
)
COLLATE='utf8_general_ci'
ENGINE=INNODB DEFAULT CHARSET=UTF8
