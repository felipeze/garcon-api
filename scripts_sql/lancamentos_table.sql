CREATE TABLE lancamentos(
	idlancamento BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	descricao VARCHAR(50) NOT NULL,
	data_vencimento DATE NOT NULL,
	data_pagamento DATE NULL DEFAULT NULL,
	valor_lancamento DECIMAL(10,2),
	valor_desconto DECIMAL(10,2),
	observacao VARCHAR(100),
	tipo VARCHAR(20) NOT NULL,
	codigo_categoria BIGINT(20) NOT NULL,
	codigo_fornecedor BIGINT(20) NOT NULL,
	INDEX(codigo_categoria),
	FOREIGN KEY(codigo_categoria) REFERENCES categorias(codigo),
	INDEX(codigo_fornecedor),
	FOREIGN KEY(codigo_fornecedor) REFERENCES fornecedores(idfornecedor)	
)COLLATE='utf8_general_ci'
ENGINE=INNODB DEFAULT CHARSET=UTF8